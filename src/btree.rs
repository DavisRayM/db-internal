//! BTree Builder

use core::panic;
use std::{
    fs::{File, OpenOptions},
    io::{BufReader, BufWriter, Read, Seek, SeekFrom, Write},
    path::PathBuf,
};

use log::trace;

use crate::{
    errors::BTreeError,
    layout::{IS_INTERNAL, IS_LEAF, PAGE_SIZE},
    traits::StorageEngine,
    PageBuilder,
};

#[derive(Debug, Clone, PartialEq)]
enum NodeType {
    Leaf,
    Internal,
}

/// On-disk B-Tree Builder
pub struct BTreeBuilder {
    root_offset: u64,
    reader: BufReader<File>,
    writer: BufWriter<File>,
}

fn node_type_to_page_kind(node_type: NodeType) -> u8 {
    match node_type {
        NodeType::Leaf => IS_LEAF,
        NodeType::Internal => IS_INTERNAL,
    }
}

type Result<T> = std::result::Result<T, BTreeError>;

impl BTreeBuilder {
    /// Create a new on-disk btree builder.
    ///
    /// This is something i'm still figuring out; The main idea is that Page is for on-memory node
    /// manipulation while this is for on-disk data traversal and modifications....
    pub fn new(source_file: PathBuf) -> Result<Self> {
        if !source_file.is_file() {
            Err(BTreeError::InvalidSourceFile {
                msg: format!("path '{:?}' is not for a valid file", source_file),
            })
        } else {
            let mut obj = Self {
                root_offset: 0,
                reader: BufReader::new(OpenOptions::new().read(true).open(&source_file).map_err(
                    |e| BTreeError::InvalidSourceFile {
                        msg: format!("failed to open source file: {}", e.to_string()),
                    },
                )?),
                writer: BufWriter::new(
                    OpenOptions::new()
                        .append(true)
                        .open(&source_file)
                        .map_err(|e| BTreeError::InvalidSourceFile {
                            msg: format!("failed to open source file: {}", e.to_string()),
                        })?,
                ),
            };

            if !obj.find_root(obj.root_offset)? {
                // NOTE: B+-Trees are always built from the bottom to the top
                // NOTE: Sooner or later I need to figure out the right-most/max index in the
                // B-Tree so that I can have an incremental entry ID
                obj.root_offset = obj.create_page(NodeType::Leaf, None);
            }
            Ok(obj)
        }
    }

    fn right_most_page(&mut self) -> PageBuilder {
        let mut page = self.load_page(self.root_offset).unwrap();
        let mut found = false;

        while !found {
            match page.page_kind() {
                IS_LEAF => found = true,
                IS_INTERNAL => {
                    page = self
                        .load_page(page.get_keys().last().unwrap().pointer() as u64)
                        .unwrap();
                }
                _ => panic!("unknown page kind"),
            }
        }

        page
    }

    /// Creates a new page on the disk source file
    ///
    /// Pages are created at the end of the file content at all times
    fn create_page(&mut self, kind: NodeType, parent_offset: Option<usize>) -> u64 {
        // TODO: Sane error handling; Complain up the stack chain
        let offset = self.writer.seek(SeekFrom::End(0)).unwrap();

        let mut page = PageBuilder::default().kind(node_type_to_page_kind(kind));
        if let Some(offset) = parent_offset {
            page = page.parent(offset);
        }

        // TODO: sane error handling
        self.writer.write_all(&mut page.get_data()).unwrap();
        offset
    }

    fn load_page(&mut self, offset: u64) -> Result<PageBuilder> {
        if let Err(e) = self.reader.seek(SeekFrom::Start(offset)) {
            trace!("failed to seek: {}", e);
            return Err(BTreeError::InvalidOffset { msg: e.to_string() });
        }

        let mut buf: [u8; PAGE_SIZE] = [0; PAGE_SIZE];
        if let Ok(len) = self.reader.read(&mut buf) {
            if len == PAGE_SIZE {
                return PageBuilder::new(buf).map_err(|e| BTreeError::InvalidOffset {
                    msg: format!("page error; {}", e.to_string()),
                });
            }
        }

        Err(BTreeError::InvalidOffset {
            msg: format!("no page at offset {}", offset),
        })
    }

    /// Searches for a root node within the source file
    fn find_root(&mut self, offset: u64) -> Result<bool> {
        match self.load_page(offset) {
            Err(BTreeError::InvalidOffset { msg: _ }) => Ok(false),
            Err(e) => Err(e),
            Ok(page) => match page.parent_offset() {
                Some(o) => self.find_root(o as u64),
                None => {
                    self.root_offset = offset;
                    Ok(true)
                }
            },
        }
    }
}

impl StorageEngine for BTreeBuilder {
    type Error = BTreeError;

    fn insert(&mut self, value: String) -> std::result::Result<usize, BTreeError> {
        todo!()
    }

    fn remove(&mut self, id: usize) -> std::result::Result<(), BTreeError> {
        todo!()
    }

    fn update(&mut self, id: usize, value: String) -> std::result::Result<usize, BTreeError> {
        todo!()
    }
}
