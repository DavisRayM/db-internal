//! Errors

use std::fmt::Display;

/// Errors that can be raised by the `PageBuilder` struct
#[derive(Debug, Clone)]
pub enum PageBuilderError {
    /// Error raised when page is invalid
    ///
    /// Usually raised when the IS_PAGE byte is not present at the beginning of page content
    InvalidPage {
        /// Detailed explanation of the error
        msg: String,
    },
    /// Error raised when the page builder is in an unexpected state
    UnexpectedState {
        /// Detailed explanation of the error
        msg: String,
    },
    /// Error raised when requested key does not exist in page
    NonExistentKey {
        /// Detailed explanation of the error
        msg: String,
    },
    /// Error raised when trying to insert a duplicate key
    KeyExists {
        /// More details tied to the error
        msg: String,
    },
    /// Error raised when page is full
    PageFull,
}

impl Display for PageBuilderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidPage { msg } => write!(f, "invalid page: {}.", msg),
            Self::UnexpectedState { msg } => write!(f, "builder in unexpected state: {}.", msg),
            Self::NonExistentKey { msg } => write!(f, "non-existent key requested: {}.", msg),
            Self::PageFull => write!(f, "action can not be performed; page is full"),
            Self::KeyExists { msg } => write!(f, "key already exists; {}.", msg),
        }
    }
}

/// Enum of possible errors that can arise when using the BTreeBuilder
#[derive(Debug, Clone)]
pub enum BTreeError {
    /// Error raised when page read by B-Tree is not valid
    PageDataError {
        /// Detailed explanation of the error
        msg: String,
    },
    /// Error raised when an invalid offset is accessed
    InvalidOffset {
        /// Detailed explanation of the error
        msg: String,
    },
    /// Invalid source file state error
    InvalidSourceFile {
        /// Detailed explanation of the error
        msg: String,
    },
}

impl Display for BTreeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidSourceFile { msg } => write!(f, "source file error: {}.", msg),
            Self::InvalidOffset { msg } => write!(f, "invalid offset: {}.", msg),
            Self::PageDataError { msg } => write!(f, "invalid page data: {}.", msg),
        }
    }
}
