use crate::{
    layout::{
        HAS_OVERFLOW, HAS_OVERFLOW_SIZE, HEADER_SIZE, IS_ROOT, NUM_CELL_SIZE, OVERFLOW_OFFSET_SIZE,
        PAGE_IS_ROOT_SIZE, PAGE_KIND_SIZE, PAGE_MAGIC_SIZE, PARENT_OFFSET_SIZE, UNSET,
        WRITE_SPACE_END_SIZE, WRITE_SPACE_START_SIZE,
    },
    traits::AsBytes,
};

/// Metadata information of a page
#[derive(Debug, Clone, PartialEq)]
pub struct PageMetadata {
    pub kind: u8,
    pub parent: Option<usize>,
    pub overflow: Option<usize>,
    pub num_cells: usize,
    pub write_space_start: usize,
    pub write_space_end: usize,
}

impl From<&[u8]> for PageMetadata {
    fn from(value: &[u8]) -> Self {
        assert!(value.len() == HEADER_SIZE - PAGE_MAGIC_SIZE);

        let kind = value[0];
        let is_root = value[1];
        let has_overflow = value[2];
        let mut offset = PAGE_KIND_SIZE + PAGE_IS_ROOT_SIZE + HAS_OVERFLOW_SIZE;

        let parent = match is_root {
            IS_ROOT => None,
            _ => Some(usize::from_be_bytes(
                value[offset..PARENT_OFFSET_SIZE + offset]
                    .try_into()
                    .unwrap(),
            )),
        };
        offset += PARENT_OFFSET_SIZE;

        let overflow = match has_overflow {
            HAS_OVERFLOW => Some(usize::from_be_bytes(
                value[offset..offset + OVERFLOW_OFFSET_SIZE]
                    .try_into()
                    .unwrap(),
            )),
            _ => None,
        };
        offset += OVERFLOW_OFFSET_SIZE;

        let num_cells =
            usize::from_be_bytes(value[offset..offset + NUM_CELL_SIZE].try_into().unwrap());
        offset += NUM_CELL_SIZE;

        let write_space_start = usize::from_be_bytes(
            value[offset..offset + WRITE_SPACE_START_SIZE]
                .try_into()
                .unwrap(),
        );
        offset += WRITE_SPACE_START_SIZE;

        let write_space_end = usize::from_be_bytes(
            value[offset..offset + WRITE_SPACE_END_SIZE]
                .try_into()
                .unwrap(),
        );

        Self {
            kind,
            parent,
            overflow,
            num_cells,
            write_space_end,
            write_space_start,
        }
    }
}

impl AsBytes for PageMetadata {
    fn as_bytes(&self) -> Vec<u8> {
        let mut bytes: Vec<u8> = Vec::with_capacity(HEADER_SIZE);

        let is_root: u8;
        let parent_offset: [u8; 8] = match self.parent {
            Some(offset) => {
                is_root = UNSET;
                offset.to_be_bytes()
            }
            None => {
                is_root = IS_ROOT;
                0_usize.to_be_bytes()
            }
        };

        let has_overflow: u8;
        let overflow_offset: [u8; 8] = match self.overflow {
            Some(offset) => {
                has_overflow = HAS_OVERFLOW;
                offset.to_be_bytes()
            }
            None => {
                has_overflow = UNSET;
                0_usize.to_be_bytes()
            }
        };

        bytes.push(self.kind);
        bytes.push(is_root);
        bytes.push(has_overflow);
        bytes.append(&mut parent_offset.to_vec());
        bytes.append(&mut overflow_offset.to_vec());
        bytes.append(&mut self.num_cells.to_be_bytes().to_vec());
        bytes.append(&mut self.write_space_start.to_be_bytes().to_vec());
        bytes.append(&mut self.write_space_end.to_be_bytes().to_vec());

        bytes
    }
}

#[cfg(test)]
mod test {
    use crate::layout::PAGE_SIZE;

    use super::*;

    #[test]
    fn metadata_as_bytes() {
        let mut flags = Vec::with_capacity(HEADER_SIZE - PAGE_MAGIC_SIZE);
        // Kind is unset
        flags.push(UNSET);
        // Is root is true
        flags.push(IS_ROOT);
        // Has overflow is unset
        flags.push(UNSET);
        // Parent offset is 0
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Overflow offset is 0
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Number of cells is 0
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Write space start is the end of the header
        flags.append(&mut HEADER_SIZE.to_be_bytes().to_vec());
        // Write space end is the end of the page
        flags.append(&mut PAGE_SIZE.to_be_bytes().to_vec());

        let meta = PageMetadata {
            kind: UNSET,
            parent: None,
            overflow: None,
            num_cells: 0,
            write_space_end: PAGE_SIZE,
            write_space_start: HEADER_SIZE,
        };

        assert_eq!(meta.as_bytes(), flags);
    }

    #[test]
    fn metadata_from_bytes() {
        let mut flags = Vec::with_capacity(HEADER_SIZE - PAGE_MAGIC_SIZE);
        // Kind is 0x03
        flags.push(0x03);
        // Is root is unset
        flags.push(UNSET);
        // Has overflow is true
        flags.push(HAS_OVERFLOW);
        // Parent offset is 203
        flags.append(&mut 203_usize.to_be_bytes().to_vec());
        // Overflow offset is 1256
        flags.append(&mut 1256_usize.to_be_bytes().to_vec());
        // Number of cells is 25
        flags.append(&mut 25_usize.to_be_bytes().to_vec());
        // Write space start is the end of the header
        flags.append(&mut HEADER_SIZE.to_be_bytes().to_vec());
        // Write space end is the end of the page
        flags.append(&mut PAGE_SIZE.to_be_bytes().to_vec());

        let expected = PageMetadata {
            kind: 0x03,
            parent: Some(203),
            overflow: Some(1256),
            num_cells: 25,
            write_space_end: PAGE_SIZE,
            write_space_start: HEADER_SIZE,
        };
        let actual = PageMetadata::from(&flags[..]);

        assert_eq!(actual, expected);
    }
}
