//! Page implementation for a database file
//!
//! A page is the highest unit a database can modify/update in it's storage file
use crate::{
    errors::PageBuilderError,
    layout::{
        Key, KeyValue, HEADER_SIZE, IS_INTERNAL, IS_LEAF, IS_PAGE, KEY_SIZE, PAGE_MAGIC_SIZE,
        PAGE_SIZE, UNSET, VALUE_SIZE,
    },
    page_metadata::PageMetadata,
    traits::AsBytes,
};

type Result<T> = std::result::Result<T, PageBuilderError>;

/// On-disk representation of a node
#[derive(Debug, Clone)]
pub struct Page([u8; PAGE_SIZE]);

#[derive(Debug, Clone)]
struct Zone {
    start_offset: usize,
    len: usize,
}

/// Page builder struct
///
/// A page builder object is responsible for modifying the contents and metadata of a page
#[derive(Debug, Clone)]
pub struct PageBuilder {
    data: Page,
    info: PageMetadata,
    unreclaimed_zones: Vec<Zone>,
}

impl PageBuilder {
    /// Create new PageBuilder object with default values
    ///
    /// ```
    /// use db_internal::page::PageBuilder;
    ///
    /// let builder = PageBuilder::default();
    /// ```
    pub fn default() -> Self {
        let mut page: [u8; PAGE_SIZE] = [0x00; PAGE_SIZE];
        page[0..PAGE_MAGIC_SIZE].clone_from_slice(&mut IS_PAGE.to_be_bytes());

        let mut obj = Self {
            data: Page(page),
            info: PageMetadata {
                kind: UNSET,
                parent: None,
                overflow: None,
                num_cells: 0,
                write_space_start: HEADER_SIZE,
                write_space_end: PAGE_SIZE,
            },
            unreclaimed_zones: Vec::new(),
        };
        obj.write_info();
        obj
    }

    /// Creates new PageBuilder object from existing page
    pub fn new(page: [u8; PAGE_SIZE]) -> Result<Self> {
        let magic = usize::from_be_bytes(page[0..PAGE_MAGIC_SIZE].try_into().map_err(|_| {
            PageBuilderError::InvalidPage {
                msg: format!("failed to read magic value bytes"),
            }
        })?);

        if magic != IS_PAGE {
            return Err(PageBuilderError::InvalidPage {
                msg: format!(
                    "byte array does not contain page data; {} != {}",
                    magic, IS_PAGE
                ),
            });
        }

        let info = PageMetadata::from(&page[PAGE_MAGIC_SIZE..HEADER_SIZE]);

        Ok(Self {
            data: Page(page),
            info,
            unreclaimed_zones: Vec::new(),
        })
    }

    /// Set page kind for the internally managed page
    ///
    /// ```
    /// use db_internal::page::PageBuilder;
    /// use db_internal::layout::IS_ROOT;
    ///
    /// let builder = PageBuilder::default().kind(IS_ROOT);
    /// ```
    pub fn kind(mut self, kind: u8) -> Self {
        self.info.kind = kind;
        self.write_info();
        self
    }

    /// Set page parent offset
    ///
    /// ```
    /// use db_internal::page::PageBuilder;
    ///
    /// let offset: usize = 100;
    /// let builder = PageBuilder::default().parent(offset);
    /// ```
    pub fn parent(mut self, offset: usize) -> Self {
        self.info.parent = Some(offset);
        self.write_info();
        self
    }

    /// Returns the parent offset of the page
    pub fn parent_offset(&self) -> Option<usize> {
        self.info.parent
    }

    /// Returns the page kind
    pub fn page_kind(&self) -> u8 {
        self.info.kind
    }

    /// Set page overflow offset
    ///
    /// ```
    /// use db_internal::page::PageBuilder;
    ///
    /// let offset: usize = 100;
    /// let builder = PageBuilder::default().overflow(offset);
    /// ```
    pub fn overflow(mut self, offset: usize) -> Self {
        self.info.overflow = Some(offset);
        self.write_info();
        self
    }

    /// Returns the quantity of free write space present in the page
    ///
    /// ```
    /// use db_internal::page::PageBuilder;
    /// use db_internal::layout::{ PAGE_SIZE, HEADER_SIZE };
    ///
    /// let builder = PageBuilder::default();
    /// assert_eq!(builder.free_space(), PAGE_SIZE - HEADER_SIZE);
    /// ```
    pub fn free_space(&self) -> usize {
        self.info.write_space_end - self.info.write_space_start
    }

    /// Returns the number of cells/offsets stored in the page
    ///
    /// ```
    /// use db_internal::page::PageBuilder;
    /// let builder = PageBuilder::default();
    ///
    /// assert_eq!(builder.num_cells(), 0);
    /// ```
    pub fn num_cells(&self) -> usize {
        self.info.num_cells
    }

    /// Returns all keys stored in page
    pub fn get_keys(&self) -> Vec<Key> {
        let mut keys = Vec::with_capacity(self.info.num_cells);
        let mut start = HEADER_SIZE;
        let key_size = KEY_SIZE;

        for _ in 0..self.info.num_cells {
            let Page(data) = self.data;
            keys.push(Key::from(&data[start..start + key_size]));
            start += key_size;
        }

        keys
    }

    /// Returns all cells stored within the page.
    ///
    /// Note: Only leaf pages have cells stored within them
    pub fn get_cells(&self) -> Vec<KeyValue> {
        if self.info.kind == IS_LEAF {
            let offsets = self.get_keys();
            offsets
                .iter()
                .map(|k| {
                    let v = self.get_cell(k.clone());
                    if let Some(v) = v {
                        v
                    } else {
                        panic!("fatal! no cell with offset: {}", k.value());
                    }
                })
                .collect::<Vec<KeyValue>>()
        } else {
            Vec::with_capacity(0)
        }
    }

    /// Returns cell at specified offset
    pub fn get_cell(&self, key: Key) -> Option<KeyValue> {
        let Page(data) = self.data;
        KeyValue::try_from(&data[key.pointer()..]).ok()
    }

    /// Reclaims marked zones in page. Returns size of space reclaimed
    ///
    /// Rewrites the data held by the page; This action modifies all cells offsets.
    pub fn reclaim_zones(&mut self) -> Result<usize> {
        match self.info.kind {
            IS_LEAF => {
                if self.unreclaimed_zones.is_empty() {
                    Ok(0)
                } else {
                    let reclaimed_space: usize = self.unreclaimed_zones.iter().map(|z| z.len).sum();

                    let cells = self
                        .get_cells()
                        .iter()
                        .map(|c| (c.key.value(), c.value.clone()))
                        .collect::<Vec<(usize, String)>>();
                    self.empty_page();
                    cells.iter().for_each(|v| {
                        self.insert_key_value(v.0, v.1.clone()).unwrap();
                    });

                    Ok(reclaimed_space)
                }
            }
            IS_INTERNAL => {
                // Internal/Index pages do not need to reclaim space for now
                Ok(0)
            }
            _ => Err(PageBuilderError::UnexpectedState {
                msg: format!("stored kind is unknown; kind = {}", self.info.kind),
            }),
        }
    }

    /// Returns a copy of the internally held page
    pub fn get_data(&self) -> [u8; PAGE_SIZE] {
        let Page(data) = self.data;
        data
    }

    /// Update a Key / KeyValue in the page. Returns updated key if KeyValue is updated
    pub fn update(
        &mut self,
        key: Option<Key>,
        value: Option<(usize, String)>,
    ) -> Result<Option<Key>> {
        if key.is_none() && value.is_none() {
            return Ok(None);
        };

        let mut keys = self.get_keys();

        if let Some(k) = key {
            if !self.key_exists(k.value()) {
                return Err(PageBuilderError::NonExistentKey {
                    msg: format!("id {} does not exist", k.value()),
                });
            }

            let pos = keys.iter().position(|v| v.value() == k.value()).unwrap();
            keys.remove(pos);
            self.write_keys(keys)?;
            Ok(None)
        } else {
            let (id, v) = value.unwrap();
            if !self.key_exists(id) {
                return Err(PageBuilderError::NonExistentKey {
                    msg: format!("id {} does not exist", id),
                });
            }

            let pos = keys.iter().position(|v| v.value() == id).unwrap();
            self.mark_deleted(
                keys[pos].pointer(),
                self.get_cell(keys[pos].clone()).unwrap().size(),
            );
            keys.remove(pos);
            self.write_keys(keys)?;

            Ok(Some(self.write_cell(v, id, None)?))
        }
    }

    /// Merges two pages together forming a new one
    pub fn merge(self, other: Self) -> Self {
        let cells = self.get_cells();
        let other_cells = other.get_cells();

        let mut new_page = Self::default();
        new_page.info.kind = self.info.kind;
        new_page.info.overflow = self.info.overflow;
        new_page.info.parent = self.info.parent;

        cells.iter().chain(other_cells.iter()).for_each(|c| {
            new_page
                .insert_key_value(c.key.value(), c.value.clone())
                .unwrap();
        });

        new_page
    }

    /// Inserts a new key/value pair
    pub fn insert_key_value(&mut self, key: usize, value: String) -> Result<Key> {
        let keys = self.get_keys();
        let v_len = value.as_bytes().len();
        let k: Key;

        if self.info.kind != IS_LEAF {
            return Err(PageBuilderError::UnexpectedState {
                msg: "value can not be written to a non leaf page!".to_string(),
            });
        } else if keys.binary_search(&Key::new(key, 0)).is_ok() {
            return Err(PageBuilderError::KeyExists {
                msg: format!("key in pair already exists"),
            });
        }

        if let Some(mut z) = self.find_zone(v_len) {
            let offset = z.start_offset;
            k = self.write_cell(value, key, Some(offset))?;

            let size = KEY_SIZE + VALUE_SIZE + v_len;
            let pos = self
                .unreclaimed_zones
                .iter()
                .position(|zone| zone.start_offset == z.start_offset)
                .unwrap();
            self.unreclaimed_zones.remove(pos);

            if z.len < size {
                z.start_offset += size;
                self.unreclaimed_zones.push(z);
            }
        } else {
            k = self.write_cell(value, key, None)?;
        }

        self.insert_key(k.clone())?;
        Ok(k)
    }

    /// Inserts a new key
    pub fn insert_key(&mut self, key: Key) -> Result<()> {
        let mut keys = self.get_keys();

        match keys.binary_search(&key) {
            Ok(_) => Err(PageBuilderError::KeyExists {
                msg: format!("{} already exists", key.value()),
            }),
            Err(pos) => {
                keys.insert(pos, key);
                self.write_keys(keys)?;
                self.write_info();
                Ok(())
            }
        }
    }

    /// Checks if key exists in page
    fn key_exists(&self, id: usize) -> bool {
        !self
            .get_keys()
            .iter()
            .filter(|k| k.value() == id)
            .collect::<Vec<&Key>>()
            .is_empty()
    }

    /// Finds a zone that can be reclaimed.
    ///
    /// Tries to find a zone that is the best fit first; size of the zone == size of the key-value pair.
    /// If such a zone does not exist it finds a zone that can store atleast two values of similar length
    fn find_zone(&mut self, v_size: usize) -> Option<Zone> {
        let actual_size = KEY_SIZE + VALUE_SIZE + v_size;
        let best_fit = self
            .unreclaimed_zones
            .iter()
            .filter(|z| z.len == actual_size)
            .map(|z| z.clone())
            .collect::<Vec<Zone>>();

        if best_fit.is_empty() {
            let next_best = self
                .unreclaimed_zones
                .iter()
                .filter(|z| z.len >= actual_size * 2)
                .map(|z| z.clone())
                .collect::<Vec<Zone>>();
            next_best.first().cloned()
        } else {
            best_fit.first().cloned()
        }
    }

    /// Removes key and associated key-value(if any) from the page.
    ///
    /// # Errors
    ///
    /// Returns an error if the specified key does not exist in the page
    pub fn remove_key(&mut self, key: Key) -> Result<()> {
        let mut keys = self.get_keys();
        let pos: usize =
            keys.iter()
                .position(|k| *k == key)
                .ok_or(PageBuilderError::NonExistentKey {
                    msg: format!("key {} does not exist in page", key.value()),
                })?;

        keys.remove(pos);
        keys.sort();
        self.write_keys(keys)?;

        if self.info.kind == IS_LEAF {
            let Page(data) = self.data;
            let v = KeyValue::try_from(&data[key.pointer()..]).unwrap();
            self.mark_deleted(key.pointer(), v.size());
        }

        self.write_info();
        Ok(())
    }

    /// Writes page info to the internally held page buffer
    fn write_info(&mut self) {
        let mut info = self.info.as_bytes();

        let Page(mut page) = self.data;
        page[PAGE_MAGIC_SIZE..HEADER_SIZE].clone_from_slice(&mut info);
        self.data = Page(page);
    }

    fn write_cell(&mut self, v: String, id: usize, offset: Option<usize>) -> Result<Key> {
        let size = KEY_SIZE + VALUE_SIZE + v.as_bytes().len();
        let start_offset: usize;

        if let Some(offset) = offset {
            start_offset = offset;
        } else {
            start_offset = self.info.write_space_end - size;
            if start_offset < self.info.write_space_start {
                return Err(PageBuilderError::PageFull {});
            }
        }

        let key = Key::new(id, start_offset);
        let kv = KeyValue::new(key.clone(), v);

        let Page(mut data) = self.data;
        data[start_offset..start_offset + size].clone_from_slice(&mut kv.as_bytes());

        self.data = Page(data);
        self.info.write_space_end = start_offset;
        Ok(key)
    }

    /// Writes a vector of Keys to the page.
    ///
    /// # Errors
    ///
    /// Returns an error if the page is full and can not accomodate the new key list
    fn write_keys(&mut self, keys: Vec<Key>) -> Result<()> {
        let Page(mut data) = self.data;
        self.info.num_cells = keys.len();
        let mut k_bytes = keys
            .iter()
            .map(|k| k.as_bytes())
            .flatten()
            .collect::<Vec<u8>>();

        let write_space_start = HEADER_SIZE + k_bytes.len();
        // TODO: I feel like this leaves one byte unoccupied; but i'll check back on this later
        if write_space_start >= self.info.write_space_end {
            return Err(PageBuilderError::PageFull {});
        }

        self.info.write_space_start = write_space_start;
        data[HEADER_SIZE..write_space_start].clone_from_slice(&mut k_bytes);

        self.data = Page(data);
        Ok(())
    }

    /// Marks a zone for reclaimation. Self().reclaim_zone() needs to be called to actually delete
    /// the occupied zone.
    fn mark_deleted(&mut self, start_offset: usize, len: usize) {
        // Increase size of adjacent zone if possible
        for z in self.unreclaimed_zones.iter_mut() {
            let end_offset = z.start_offset + z.len;
            if end_offset == start_offset {
                z.len += len;
                return;
            }

            let end_offset = start_offset + len;
            if end_offset == z.start_offset {
                z.start_offset = start_offset;
                z.len += len;
                return;
            }
        }

        self.unreclaimed_zones.push(Zone { start_offset, len });
    }

    /// Deletes/Invalidates all content in the page
    fn empty_page(&mut self) {
        let mut page: [u8; PAGE_SIZE] = [0x00; PAGE_SIZE];
        page[0..PAGE_MAGIC_SIZE].clone_from_slice(&mut IS_PAGE.to_be_bytes());

        self.data = Page(page);
        self.info = PageMetadata {
            kind: self.info.kind,
            parent: self.info.parent,
            overflow: self.info.overflow,
            num_cells: 0,
            write_space_start: HEADER_SIZE,
            write_space_end: PAGE_SIZE,
        };
        self.unreclaimed_zones = Vec::new();
        self.write_info();
    }
}

#[cfg(test)]
mod test {
    use crate::layout::IS_ROOT;

    use super::*;

    fn default_page() -> [u8; PAGE_SIZE] {
        let mut flags = IS_PAGE.to_be_bytes().to_vec();
        // Kind is unset
        flags.push(UNSET);
        // Is root is true
        flags.push(IS_ROOT);
        // Has overflow is unset
        flags.push(UNSET);
        // Parent offset is 0 by default
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Overflow offset is 0 by default
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Number of cells is 0 by default
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Write space start is the end of the header
        flags.append(&mut HEADER_SIZE.to_be_bytes().to_vec());
        // Write space end is the end of the page
        flags.append(&mut PAGE_SIZE.to_be_bytes().to_vec());

        let junk: [u8; PAGE_SIZE - HEADER_SIZE] = [0x00; PAGE_SIZE - HEADER_SIZE];

        let mut data: [u8; PAGE_SIZE] = [0x00; PAGE_SIZE];

        for (to, from) in data.iter_mut().zip(flags.iter().chain(junk.iter())) {
            *to = *from;
        }

        data
    }

    fn full_page() -> [u8; PAGE_SIZE] {
        let mut flags = IS_PAGE.to_be_bytes().to_vec();
        // Kind is unset
        flags.push(UNSET);
        // Is root is true
        flags.push(IS_ROOT);
        // Has overflow is unset
        flags.push(UNSET);
        // Parent offset is 0 by default
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Overflow offset is 0 by default
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Number of cells is 0 by default
        flags.append(&mut 0_usize.to_be_bytes().to_vec());
        // Write space start is the end of the header
        flags.append(&mut HEADER_SIZE.to_be_bytes().to_vec());
        // Write space end is the end of the page
        flags.append(&mut HEADER_SIZE.to_be_bytes().to_vec());

        let junk: [u8; PAGE_SIZE - HEADER_SIZE] = [0x00; PAGE_SIZE - HEADER_SIZE];

        let mut data: [u8; PAGE_SIZE] = [0x11; PAGE_SIZE];

        for (to, from) in data.iter_mut().zip(flags.iter().chain(junk.iter())) {
            *to = *from;
        }

        data
    }

    #[test]
    fn default_page_as_expected() {
        let page = PageBuilder::default().get_data();
        assert_eq!(page, default_page());
    }

    #[test]
    fn page_from_existing_data() {
        let builder = PageBuilder::new(default_page()).unwrap();

        assert_eq!(builder.info.kind, UNSET);
        assert_eq!(builder.info.num_cells, 0);
        assert_eq!(builder.info.parent, None);
        assert_eq!(builder.info.overflow, None);
        assert_eq!(builder.info.write_space_start, HEADER_SIZE);
        assert_eq!(builder.info.write_space_end, PAGE_SIZE);
    }

    #[test]
    #[should_panic(expected = "byte array does not contain page data")]
    fn builder_invalid_page() {
        let junk: [u8; PAGE_SIZE] = [0x99; PAGE_SIZE];
        PageBuilder::new(junk).unwrap();
    }

    #[test]
    #[should_panic(expected = "PageFull")]
    fn write_key_to_full_page() {
        let mut builder = PageBuilder::new(full_page()).unwrap();
        builder.insert_key(Key::new(25, 25)).unwrap();
    }

    #[test]
    fn page_write_key() {
        let mut builder = PageBuilder::default();

        builder.insert_key(Key::new(25, 123)).unwrap();

        let mut d = IS_PAGE.to_be_bytes().to_vec();
        // Kind is unset
        d.push(UNSET);
        // Is root is true
        d.push(IS_ROOT);
        // Has overflow is unset
        d.push(UNSET);
        // Parent offset is 0
        d.append(&mut 0_usize.to_be_bytes().to_vec());
        // Overflow offset is 0
        d.append(&mut 0_usize.to_be_bytes().to_vec());
        // Number of cells is 0
        d.append(&mut 1_usize.to_be_bytes().to_vec());
        // Write space start is the end of the header
        d.append(&mut (HEADER_SIZE + KEY_SIZE).to_be_bytes().to_vec());
        // Write space end is the end of the page
        d.append(&mut PAGE_SIZE.to_be_bytes().to_vec());
        d.append(&mut 25_usize.to_be_bytes().to_vec());
        d.append(&mut 123_usize.to_be_bytes().to_vec());

        let junk: [u8; PAGE_SIZE - HEADER_SIZE - KEY_SIZE] =
            [0x00; PAGE_SIZE - HEADER_SIZE - KEY_SIZE];

        let mut data: [u8; PAGE_SIZE] = [0x11; PAGE_SIZE];
        for (to, from) in data.iter_mut().zip(d.iter().chain(junk.iter())) {
            *to = *from;
        }

        assert_eq!(builder.info.num_cells, 1);
        assert_eq!(builder.info.write_space_start, HEADER_SIZE + KEY_SIZE);
        assert_eq!(builder.info.write_space_end, PAGE_SIZE);
        assert_eq!(builder.get_keys(), vec![Key::new(25, 123)]);
        assert_eq!(builder.get_data(), data);
    }

    #[test]
    fn page_keys_sorted() {
        let mut builder = PageBuilder::default();

        // Add keys in random order
        builder.insert_key(Key::new(36, 123)).unwrap();
        builder.insert_key(Key::new(25, 123)).unwrap();
        builder.insert_key(Key::new(30, 123)).unwrap();

        let mut d = IS_PAGE.to_be_bytes().to_vec();
        // Kind is unset
        d.push(UNSET);
        // Is root is true
        d.push(IS_ROOT);
        // Has overflow is unset
        d.push(UNSET);
        // Parent offset is 0
        d.append(&mut 0_usize.to_be_bytes().to_vec());
        // Overflow offset is 0
        d.append(&mut 0_usize.to_be_bytes().to_vec());
        // Number of cells is 0
        d.append(&mut 3_usize.to_be_bytes().to_vec());
        // Write space start is the end of the header
        d.append(&mut (HEADER_SIZE + (KEY_SIZE * 3)).to_be_bytes().to_vec());
        // Write space end is the end of the page
        d.append(&mut PAGE_SIZE.to_be_bytes().to_vec());

        // Keys should be after metadata
        d.append(&mut 25_usize.to_be_bytes().to_vec());
        d.append(&mut 123_usize.to_be_bytes().to_vec());
        d.append(&mut 30_usize.to_be_bytes().to_vec());
        d.append(&mut 123_usize.to_be_bytes().to_vec());
        d.append(&mut 36_usize.to_be_bytes().to_vec());
        d.append(&mut 123_usize.to_be_bytes().to_vec());

        let junk: [u8; PAGE_SIZE - HEADER_SIZE - (KEY_SIZE * 3)] =
            [0x00; PAGE_SIZE - HEADER_SIZE - (KEY_SIZE * 3)];

        let mut data: [u8; PAGE_SIZE] = [0x11; PAGE_SIZE];
        for (to, from) in data.iter_mut().zip(d.iter().chain(junk.iter())) {
            *to = *from;
        }

        assert_eq!(builder.info.num_cells, 3);
        assert_eq!(builder.info.write_space_start, HEADER_SIZE + (KEY_SIZE * 3));
        assert_eq!(builder.info.write_space_end, PAGE_SIZE);
        assert_eq!(
            builder.get_keys(),
            vec![Key::new(25, 123), Key::new(30, 123), Key::new(36, 123)]
        );
        assert_eq!(builder.get_data(), data);
    }

    #[test]
    #[should_panic(expected = "PageFull")]
    fn write_cell_to_full_page() {
        let mut builder = PageBuilder::new(full_page()).unwrap().kind(IS_LEAF);
        builder.insert_key_value(1, "Data".to_string()).unwrap();
    }

    #[test]
    fn write_cell_to_page() {
        let mut builder = PageBuilder::new(default_page()).unwrap().kind(IS_LEAF);
        let prev_write_space_end = builder.info.write_space_end;

        builder
            .insert_key_value(1, "lorem ipsum".to_string())
            .unwrap();
        assert_eq!(builder.get_keys().len(), 1);
        assert_eq!(builder.get_cells().len(), 1);
        assert_eq!(builder.info.num_cells, 1);
        assert!(prev_write_space_end > builder.info.write_space_end);
    }

    #[test]
    fn write_cell_reclaims_space() {
        let mut builder = PageBuilder::new(default_page()).unwrap().kind(IS_LEAF);

        let key = builder
            .insert_key_value(1, "lorem ipsum".to_string())
            .unwrap();
        let prev_write_space_end = builder.info.write_space_end;

        builder.remove_key(key).unwrap();

        assert_eq!(builder.unreclaimed_zones.len(), 1);
        assert_eq!(builder.get_keys().len(), 0);
        assert_eq!(builder.get_cells().len(), 0);
        assert_eq!(builder.info.num_cells, 0);

        builder
            .insert_key_value(1, "lorem ipsum".to_string())
            .unwrap();
        assert_eq!(builder.unreclaimed_zones.len(), 0);
        assert_eq!(builder.get_keys().len(), 1);
        assert_eq!(builder.get_cells().len(), 1);
        assert_eq!(builder.info.num_cells, 1);
        assert_eq!(builder.info.write_space_end, prev_write_space_end);
    }

    #[test]
    #[should_panic(expected = "25 already exists")]
    fn write_key_duplicate_fails() {
        let mut builder = PageBuilder::default();

        builder.insert_key(Key::new(25, 123)).unwrap();
        builder.insert_key(Key::new(25, 2503)).unwrap();
    }

    #[test]
    fn page_reclaim_space() {
        let mut builder = PageBuilder::new(default_page()).unwrap().kind(IS_LEAF);

        let _ = builder
            .insert_key_value(1, "lorem ipsum".to_string())
            .unwrap();
        let k2 = builder
            .insert_key_value(2, "some ipsum".to_string())
            .unwrap();
        let _ = builder
            .insert_key_value(3, "always ipsum".to_string())
            .unwrap();

        builder.remove_key(k2).unwrap();
        assert_eq!(builder.unreclaimed_zones.len(), 1);

        builder.reclaim_zones().unwrap();
        assert_eq!(builder.unreclaimed_zones.len(), 0);
        assert_eq!(builder.info.num_cells, 2);
        assert_eq!(builder.info.kind, IS_LEAF);
    }
}
