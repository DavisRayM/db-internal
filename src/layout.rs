//! Page layout utilities, types and flags

use std::mem::size_of;

use crate::traits::AsBytes;
/// Flag unset value
pub const UNSET: u8 = 0x99;

// Node flags
/// Node is root
pub const IS_ROOT: u8 = 0x0;
/// Node is leaf
pub const IS_LEAF: u8 = 0x1;
/// Node is internal
pub const IS_INTERNAL: u8 = 0x2;

// Cell layout
/// Size of a page ID within a cell
pub const PAGE_ID_SIZE: usize = size_of::<usize>();
/// Size of a key within a cell
pub const KEY_SIZE: usize = size_of::<usize>() + PAGE_ID_SIZE;
/// Size of value stored within cell
pub const VALUE_SIZE: usize = size_of::<usize>();

// Page flags
/// Magic number specifying that the block of data is a page
pub const IS_PAGE: usize = 0xfbce;
/// Whether the page has an overflow page
pub const HAS_OVERFLOW: u8 = 0x3;

// Page layout
/// Maximum size of a page
pub const PAGE_SIZE: usize = 4094;
/// Size taken by the page magic number
pub const PAGE_MAGIC_SIZE: usize = size_of::<usize>();
/// Size taken by the page is root metadata
pub const PAGE_IS_ROOT_SIZE: usize = size_of::<u8>();
/// Size taken by the page kind metadata
pub const PAGE_KIND_SIZE: usize = size_of::<u8>();
/// Size taken by the page number of cells metadata
pub const NUM_CELL_SIZE: usize = size_of::<usize>();
/// Size taken by the page overflow offset metadata
pub const OVERFLOW_OFFSET_SIZE: usize = size_of::<usize>();
/// Size taken by the page parent offset metadata
pub const PARENT_OFFSET_SIZE: usize = size_of::<usize>();
/// Size taken by the has overflow flag
pub const HAS_OVERFLOW_SIZE: usize = size_of::<u8>();
/// Size taken by the free space start offset metadata
pub const WRITE_SPACE_START_SIZE: usize = size_of::<usize>();
/// Size taken by the free space end offset metadata
pub const WRITE_SPACE_END_SIZE: usize = size_of::<usize>();

/// Size of a page's header
pub const HEADER_SIZE: usize = PAGE_MAGIC_SIZE
    + PAGE_IS_ROOT_SIZE
    + PARENT_OFFSET_SIZE
    + HAS_OVERFLOW_SIZE
    + OVERFLOW_OFFSET_SIZE
    + PAGE_KIND_SIZE
    + NUM_CELL_SIZE
    + WRITE_SPACE_START_SIZE
    + WRITE_SPACE_END_SIZE;

/// Maximum size of content that can be stored in a page
pub const MAX_CONTENT_SIZE: usize = PAGE_SIZE - HEADER_SIZE;

/// Key/Offset of a value stored in the database
#[derive(Debug, Clone, Eq)]
pub struct Key {
    /// Unique identifier
    key: usize,
    /// Pointer to associated value; This is a page ID in internal nodes while in leaf nodes this
    /// is the offset of the associated value
    pointer: usize,
}

impl PartialOrd for Key {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.key.partial_cmp(&other.key)
    }
}

impl Ord for Key {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.key.cmp(&other.key)
    }
}

impl PartialEq for Key {
    fn eq(&self, other: &Self) -> bool {
        self.key == other.key
    }
}

impl Key {
    /// Creates a new key
    ///
    /// ```
    /// use db_internal::layout::Key;
    /// let key = Key::new(100, 232349);
    /// ```
    pub fn new(v: usize, points_to: usize) -> Self {
        Self {
            key: v,
            pointer: points_to,
        }
    }

    /// Returns the value of the key
    pub fn value(&self) -> usize {
        self.key
    }

    /// Returns the page ID the key points to
    pub fn pointer(&self) -> usize {
        self.pointer
    }

    /// Returns the size of key bytes
    pub fn size(&self) -> usize {
        KEY_SIZE
    }
}

impl From<&[u8]> for Key {
    fn from(value: &[u8]) -> Self {
        assert!(value.len() >= KEY_SIZE);

        let offset = usize::from_be_bytes(value[0..KEY_SIZE - PAGE_ID_SIZE].try_into().unwrap());
        let points_to =
            usize::from_be_bytes(value[KEY_SIZE - PAGE_ID_SIZE..KEY_SIZE].try_into().unwrap());

        Self {
            key: offset,
            pointer: points_to,
        }
    }
}

impl AsBytes for Key {
    fn as_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::with_capacity(self.size());

        bytes.append(&mut self.key.to_be_bytes().to_vec());
        bytes.append(&mut self.pointer.to_be_bytes().to_vec());

        bytes
    }
}

/// Key-Value pair stored in page cells
#[derive(Debug, Clone, PartialEq)]
pub struct KeyValue {
    /// Key value
    pub key: Key,
    /// Stored value
    pub value: String,
}

impl KeyValue {
    /// Creates a new key value
    ///
    /// ```
    /// use db_internal::layout::KeyValue;
    /// use db_internal::layout::Key;
    /// let kv = KeyValue::new(Key::new(20, 232323), "sup".to_string());
    /// ```
    pub fn new(k: Key, v: String) -> Self {
        Self { key: k, value: v }
    }

    /// Returns the size of key value bytes
    pub fn size(&self) -> usize {
        return self.key.size() + VALUE_SIZE + self.value.as_bytes().len();
    }
}

impl TryFrom<&[u8]> for KeyValue {
    type Error = String;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        if value.len() < KEY_SIZE + VALUE_SIZE {
            Err("value is not valid".to_string())
        } else {
            let key = Key::from(&value[0..KEY_SIZE]);
            let v_size =
                usize::from_be_bytes(value[KEY_SIZE..KEY_SIZE + VALUE_SIZE].try_into().unwrap());
            let v = String::from_utf8(
                value[KEY_SIZE + VALUE_SIZE..KEY_SIZE + VALUE_SIZE + v_size].to_vec(),
            )
            .unwrap();

            Ok(Self { key, value: v })
        }
    }
}

impl AsBytes for KeyValue {
    fn as_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::with_capacity(self.size());

        bytes.append(&mut self.key.as_bytes());

        let mut v_bytes = self.value.as_bytes().to_vec();
        let mut v_len = v_bytes.len().to_be_bytes().to_vec();

        bytes.append(&mut v_len);
        bytes.append(&mut v_bytes);

        bytes
    }
}
