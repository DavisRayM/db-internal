//! Traits that can be implemented within the project

/// AsBytes trait; Objects that implement this trait can be represented as byte arrays
pub trait AsBytes {
    /// Returns the byte representation of the value
    fn as_bytes(&self) -> Vec<u8>;
}

/// Trait implemented by storage engines
pub trait StorageEngine {
    type Error;

    /// Insert a new entry into storage. Returns the unique ID of the entry
    fn insert(&mut self, value: String) -> Result<usize, Self::Error>;
    /// Remove a specific entry
    ///
    fn remove(&mut self, id: usize) -> Result<(), Self::Error>;
    /// Update a specific entry
    ///
    fn update(&mut self, id: usize, value: String) -> Result<usize, Self::Error>;
}
