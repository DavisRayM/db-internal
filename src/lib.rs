//! Disk based storage structure library
//!
//! Contains structures and methods that are useful for creating an on-disk storage based engine
#![deny(missing_docs)]
pub mod btree;
pub mod errors;
pub mod layout;
pub mod page;
mod page_metadata;
mod traits;

pub use btree::BTreeBuilder;
pub use page::PageBuilder;
