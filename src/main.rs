use db_internal::layout::*;

fn main() {
    println!(
        "MAX_PAGE_SIZE: {PAGE_SIZE}\nHEADER SIZE: {HEADER_SIZE}\nCONTENT SIZE: {MAX_CONTENT_SIZE}"
    );
}
